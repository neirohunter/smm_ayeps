import './scss/style.scss';
import './js/sprite-svg';
// import './js/swiper.min.js';
import App from './App';

const app = new App();

app.init();
