# Скелет верстки
-------
Базовый скелет для верстки/фронтенд части, основаный на Webpack 4 и Typescript. Для поддержки единого стиля 
используется сконфигурированный линтер ts-lint. Прохождение проверки стилистики кода - обязательно!

**Структура**

`src` - исходники проекта.

`dist` - директория файлов для продакшн - содержит минифицированные версии стилей и javascript, изображения 
(Не править напрямую).

`src/ts` - исходники typescript.

`src/scss` - исходники sass.

`src/images` - исходники - изображения проекта.

`src/fonts` - подключаемые шрифты.

`src/images/svg-sprite` - исходники svg для svg-спрайтов (и только для спрйтов !), подключаются в единый файл 
`ts/sprite-svg.js` через `import`.

**Как пользоваться**

1. Установка `npm` пакетов командой `npm install`.

2. Запуск дев-сервера командой `npm run dev`. Дев сервер будет отслеживать изменения в файлах, 
используемых в проекте и автоматически пересобирать проект.

3. Сборка проекта осуществляется командой `npm run build`.

## Перечень основных правил стилистики кода

Использование одинарных кавычек `'`.

Обязательное использование `;`.

## Требования по верстке и стилистике css

Работа ведется строго через sass-препроцессор.

Правильное подключение шрифтов: объединь семейство шрифтов одним названием

```
@font-face {
  font-family: 'Ubuntu';
  font-style: normal;
  font-weight: 400;
  src: url(regular.woff2) format('woff2');
}
@font-face {
  font-family: 'Ubuntu';
  font-style: normal;
  font-weight: 700;
  src: url(bold.woff2) format('woff2');
}
```
Пример выше показывает как подключены 2 разных файла шрифтов по толщине.

Либо использовать импорт шрифтов из сервиса google fonts.

При написании селекторов придерживаться стилистики БЭМ.

Стараться избегать вложенности и каскадирования стилей (опционально).

В качестве сетки можно использовать встроенный бутстрап4. (flex, забудьте про старье в виде float)

Придерживаться внятной архитектуры директорий и файлов,
разделять стили по эелементам и секциям.

Пример:

`abstract` - содержит миксины и переменные используемые в стилях

`components/buttons` - содержит стили для кнопок.

`layout` - стили лэйаутов, footer и header

`section` - разделение стилей по логичным секциям.

Пути к изображениям, шрифтам задается напрямую как `images/img.png` и `fonts/some-font.ttf`. 
Для этого установлен loader `resolve-url-loader`, который решает проблему с путями при сборке.

## JavaScript

Используем возможности ES6 и полифилы для всех необходимых функций (core-js либо babel-polyfill). 
[Подробнее](https://learn.javascript.ru/es-modern)

Объявление переменных через `const` и `let`. [Подробнее](https://learn.javascript.ru/let-const)

Классы. [Подробнее](https://learn.javascript.ru/es-class)

### Рекомендации по часто-используемым библиотекам

##### Utility (Полезные функции)
 
[lodash](https://lodash.com/)

[underscore](http://underscorejs.ru/)

#### Слайдеры

При использовании в проекте jQuery:
[slick-slider](http://kenwheeler.github.io/slick/) (включен в данную сборку по-умолчанию)

Если jQuery не используется: [swiper](http://idangero.us/swiper/) (включен в данную сборку по-умолчанию)

#### XHR (XMLHttpRequest)

При использовании jQuery - [ajax](http://api.jquery.com/jquery.ajax/) (и думать нечего).

[axios](https://github.com/axios/axios) (включен в данную сборку по-умолчанию)

[Fetch_API](https://developer.mozilla.org/ru/docs/Web/API/Fetch_API/Using_Fetch) и его 
[polyfill](https://github.com/github/fetch) (включен в данную сборку по-умолчанию)

#### Маска ввода

[inputmask](https://github.com/RobinHerbots/Inputmask) (включен в данную сборку по-умолчанию)

#### Чарты и графики

[highcharts](https://www.highcharts.com/)

[echarts](https://ecomfe.github.io/echarts-examples/public/index.html)

[chartjs](http://www.chartjs.org/)

#### Анимация

[gsap](https://greensock.com/tweenmax) (Практически стандарт анимации в вебе).

#### Модальные окна и уведомления

[izimodal](http://izimodal.marcelodolce.com/)

[sweetalert](https://sweetalert2.github.io/)

[alertify](http://alertifyjs.com/)

#### Лайтбокс ( всплывающие галлереи/изображения)

[Lightgallery](https://sachinchoolur.github.io/lightgallery.js/)

[magnific popup](http://dimsemenov.com/plugins/magnific-popup/)

#### Скроллеры

[perfect-scrollbar](https://github.com/utatti/perfect-scrollbar)

[jquery.nicescroll](https://github.com/inuyaksa/jquery.nicescroll)

#### Поддержка старых браузеров

[modernizr](https://modernizr.com/)