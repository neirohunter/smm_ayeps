const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const webpack = require('webpack');
const path = require("path");

module.exports = {
  entry: './src/index.js',
  resolve: {
    extensions: ['.ts', ".js", ".json"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!(dom7|swiper)\/).*/,
        use: {
          loader: "babel-loader"
        }
      },
      // {
      //   test: /\.tsx?$/,
      //   use: [
      //     {
      //       loader: "js-loader"
      //     },
      //     {
      //       loader: "tslint-loader",
      //       options: {
      //         emitErrors: true
      //       }
      //     }
      //   ]
      // },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader",
            options: {
              sourceMap:true,
            }// creates style nodes from JS strings
          },
          {
            loader: "css-loader",
            options: {
              sourceMap:true,
            }// translates CSS into CommonJS
          },
          {
            loader: 'resolve-url-loader',
            options: {
              debug: true,
              root: './src',
              includeRoot: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap:true,
            }// compiles Sass to CSS
          }
        ]
      },
      {
        test: /\.svg$/,
        include: [
          path.resolve(__dirname, "src/images/svg-sprite")
        ],
        loader: 'svg-sprite-loader'
      },
      {
        test: /\.(eot|svg|ttf|otf|woff|woff2)$/,
        include: [
          path.resolve(__dirname, "src/fonts")
        ],
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1000, // Convert images < 1kb to base64 strings
            name: 'fonts/[hash]-[name].[ext]'
          }
        }]
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        exclude:[path.resolve(__dirname, "src/images/svg-sprite"), path.resolve(__dirname, "src/fonts")],
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1000, // Convert images < 1kb to base64 strings
            name: 'images/[hash]-[name].[ext]'
          }
        }]
      }
    ]
  },
  plugins: [
    new BundleAnalyzerPlugin(),
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
    }),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]
};
