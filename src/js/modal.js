export default class AypModal {
  constructor(debug = false) {
    const self = this;
    this.debugMode = debug;
    this.modalActiveClass = 'modal--is-visible';
    this.overlayClass = '.overlay';
    this.overlayIsVisibleClass = 'overlay--is-visible';
    this.overlay =  document.querySelector(this.overlayClass);
    this.body =  document.querySelector('body');
    this.overlay.onclick = function(e) {
      if (!e.target.closest('.modal__inner')) {
        self.closeModal();
      }
    };
    this.closeEvent = new CustomEvent('modal-close');
  }

  openModal = (id) => {
    const modalWin =  document.getElementById(id);
    this.hideActive();
    if (!modalWin) {
      if (this.debugMode) {
        console.log('Не удалось найти элемент с id: ' + id);
      }
      return;
    }
    const h = modalWin.offsetHeight;
    this.showOverlay();
    if (h < window.innerHeight ) {
      modalWin.style.top = this.calculateTop(h, window.innerHeight) + 'px';
    } else {
      modalWin.style.top = '10px';
    }
    this.body.style.overflow = 'hidden';
    modalWin.classList.add(this.modalActiveClass);
  }

   showOverlay = () => {
    this.overlay.classList.add(this.overlayIsVisibleClass);
  }

  closeModal = () => {
    this.overlay.classList.remove(this.overlayIsVisibleClass);
    this.hideActive();
    this.overlay.dispatchEvent(this.closeEvent);
    this.body.style.overflow = 'visible';
  }

   calculateTop = (modalH, winH) => {
    return (winH - modalH) / 2;
  }

   hideActive = () => {
    const activeModal = document.querySelector('.' + this.modalActiveClass);
    if (activeModal !== null) {
      activeModal.classList.remove(this.modalActiveClass);
    }
  }
}
