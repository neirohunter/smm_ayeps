const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const path = require("path");
const pathsToClean = [
  'dist'
];

module.exports = {
  entry: './src/index.js',
  resolve: {
    extensions: ['.ts', ".js", ".json"]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[hash].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
        use: {
          loader: "babel-loader"
        }
      },
      // {
      //   test: /\.js?$/,
      //   use: [
      //     {
      //       loader: "babel-loader"
      //     },
      //     {
      //       loader: "js-loader"
      //     },
      //     {
      //       loader: "tslint-loader",
      //       options: {
      //         emitErrors: true
      //       }
      //     }
      //   ]
      // },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: false }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              minimize: true,
              sourceMap:false,
            }
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap:false,
              plugins:[
                require('autoprefixer')(),
              ]
            }
          },
          {
            loader: 'resolve-url-loader',
            options: {
              debug: true,
              root: './src',
              includeRoot: true,
            },
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              minimize: true,
              sourceMap:false,
            }
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap:false,
              plugins:[
                require('autoprefixer')(),
              ]
            }
          },
          {
            loader: 'resolve-url-loader',
            options: {
              debug: true,
              root: './src',
              includeRoot: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap:true,
            }// compiles Sass to CSS
          }
        ]
      },
      {
        test: /\.svg$/,
        include: [
          path.resolve(__dirname, "src/images/svg-sprite")
        ],
        loader: 'svg-sprite-loader'
      },
      {
        test: /\.(eot|svg|ttf|otf|woff|woff2)$/,
        include: [
          path.resolve(__dirname, "src/fonts")
        ],
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1000, // Convert images < 1kb to base64 strings
            name: 'fonts/[hash]-[name].[ext]'
          }
        }]
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        exclude:[path.resolve(__dirname, "src/images/svg-sprite"), path.resolve(__dirname, "src/fonts")],
        use: [{
          loader: 'url-loader',
          options: {
            limit: 1000, // Convert images < 1kb to base64 strings
            name: 'images/[hash]-[name].[ext]'
          }
        }]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(pathsToClean),
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
    }),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
      chunkFilename: "[id].css"
    })
  ]
};
