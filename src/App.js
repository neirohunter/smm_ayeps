// import './js/Utils';
import Swiper from './js/swiper/swiper';
import Modal from './js/modal';
import Inputmask from 'inputmask';

export default class App {
    init = () => {
        this.stages();
        this.mapBox();
        this.modal();
        this.menuBlock();
        this.toggleSlider();
    };

    stages = () => {
        $(document).ready(function() {
            let dataSlide = $('.js-stages-slider__slide');
            let swiper = new Swiper('.js-swiper-stages', {
                slidesPerView: 1,
                spaceBetween: 30,
                slidesPerGroup: 1,
                loop: true,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: '.js-stages-slider__pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                      return '<span class="' + className + ' stages-slider__pagination-bulet">' + dataSlide.eq(index).attr('data-slider-descr') + '</span>';
                    }
                },
                navigation: {
                    nextEl: '.stages-slider__next',
                    prevEl: '.stages-slider__prew',
                },
            });

            let stagesMobile = new Swiper('.js-swiper-stages-mobile', {
                slidesPerView: 1,
                spaceBetween: 30,
                slidesPerGroup: 1,
                loop: true,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: '.js-stages-slider__pagination-mobile',
                    clickable: true,
                    renderBullet: function (index, className) {
                      return '<span class="' + className + ' stages-slider__pagination-bulet">' + (index + 1) + '</span>';
                    }
                },
                navigation: {
                    nextEl: '.stages-slider__next',
                    prevEl: '.stages-slider__prew',
                },
            });

            let codex = new Swiper('.js-swiper-codex', {
                slidesPerView: 1,
                spaceBetween: 30,
                slidesPerGroup: 1,
                loop: true,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: '.codex-slider__pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                      return '<span class="' + className + ' codex-slider__pagination-bulet">' + (index + 1) + '</span>';
                    }
                },
                navigation: {
                    nextEl: '.codex-slider__next',
                    prevEl: '.codex-slider__prew',
                },
            });

            let rewiev = new Swiper('.js-swiper-rewiev', {
                slidesPerView: 1,
                spaceBetween: 30,
                slidesPerGroup: 1,
                loop: true,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: '.rewiev-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.rewiev-slider__next',
                    prevEl: '.rewiev-slider__prew',
                },
            });
        });
    }

    mapBox = () => {
        let coordinates = {lat: 55.78955484, lng: 37.7098686},
            map = new google.maps.Map(document.getElementById('map'), {
                center: coordinates,
                zoom: 17
            });

        let marker = new google.maps.Marker({
            map: map,
            position: coordinates,
        });

        let geocoder = new google.maps.Geocoder();

        // geocodeAddress(geocoder, map, '107023, Россия, г. Москва, ул. Суворовская д. 19/9 БЦ "Галатекс"');

        function geocodeAddress(geocoder, resultsMap, address) {
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === 'OK') {
                    resultsMap.setCenter({lat: results[0].geometry.location.lat() + 0.0005, lng: results[0].geometry.location.lng() + 0.0035 });

                    let marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location,
                    });

                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        $.getJSON("/src/js/json/google-map-style.json", function(data) {
            map.setOptions({styles: data});
        });
    }

    modal = () => {
        const modal = new Modal();
        const im = new Inputmask('+7(999) 999-99-99');

        im.mask($('input[name="phone"]'));

        $('.js-modal-open').on('click', function() {
            let targetId = $(this).data('modal');
          modal.openModal(targetId);
        });
        $('.js-modal-close').click(function (e) {
          e.preventDefault();
          modal.closeModal();
        });
    }

    menuBlock = () => {
        $('.js-menu-open').on('click', function() {
            $('.nav-mobile__menu-box').slideToggle(500);
        });
        $('.js-menu-close, .nav-mobile__item').on('click', function() {
            $('.nav-mobile__menu-box').slideToggle(500);
        });
    }

    toggleSlider = () => {
        $('.js-work-popup-button').on('click', function() {
            if (!$(this).hasClass('active')){
                $('.js-work-popup-button').removeClass('active').find('.js-work-popup').slideUp(250);
                $(this).addClass('active').find('.js-work-popup').slideDown(250);
            } else {
                $(this).removeClass('active').find('.js-work-popup').slideUp(250);
            }
        });
        $('.js-close-button-work').on('click', function(){
            $(this).parent('.js-work-popup').slideDown(250);
        });
    }
}
